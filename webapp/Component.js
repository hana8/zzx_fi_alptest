jQuery.sap.declare("cdstest1.ZZX_FI_ALPTEST1.Component");
sap.ui.getCore().loadLibrary("sap.ui.generic.app");
jQuery.sap.require("sap.ui.generic.app.AppComponent");

sap.ui.generic.app.AppComponent.extend("cdstest1.ZZX_FI_ALPTEST1.Component", {
	metadata: {
		"manifest": "json"
	}
});